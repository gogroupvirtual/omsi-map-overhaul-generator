**Table of contents**<br />

[[_TOC_]]

# GO Group Virtual OMSI 2 Tool

This a small script/tool that allows you to automatically generate your favourite flavour of our map overhaul mod for our supported maps.

![Preview of the tool](https://i.imgur.com/2nvQEwp.png)

## Features
- Install map overhaul mods for supported maps
- Optional map specific extra options
    - In case of Ahlheim & Laurenzbach it asks if you have extra lines addons installed so it knows if it needs those files also
- Uninstall map overhaul mod for supported maps
- Select if you want install another driver or passenger variations for the selected map
- Select which buses you want for each avilable category for the selected map
- Select AI traffic pack flavours (or keep original) for supported maps
- Depot files sync script (manual and after map overhaul adding) option
- Automatic data folder update checker and downloader

# Getting, updating the script
1. Download the latest package from the releases: https://gitlab.com/gogroupvirtual/omsi-map-overhaul-generator/-/releases
2. If updating, put in the same folder as old exe/files, overwriting the old files. 

# Uninstall
1. Uninstall all map overhaul's you have installed via the tool
2. Delete the folder where your .exe is located
3. Delete this folder: `%appdata%\GO Group Virtual OMSI Tool`

# Early version guidelines
- As there still bug fixes even inside the data (alists, etc), **I recommend recreating the overhaul every time you do a trip on the map** (if you see there have been any Data updates in #omsi-tool-releases)
- If you have second monitor, keep monitor OMSI log file. To do this:
  - `Shift` + `Right click` in an empty space inside in your OMSI 2 folder
  - Choose Open Powershell window here
  - Write command `Get-Content logfile.txt –Wait`
  - Voila, you'll now see OMSI logs live.
  - Especially keep out for errors like:
    - `Warning:       Line "maps\Ahlheim_Laurenzbach Updated\Chrono\GO Group Virtual\TTData\SLZ_820A.ttl", tour 3 has at least one invalid trip index (Nr. 1)!` on map load
    - `Error:           The file "vehicles\HH_EBus2019\HHStadtgelenkbus2017_77_main.bus" could not be loaded!`

# Supported maps
- Ahlheim & Laureznbach Updated
- Fictional Szcezin
- Grande Porto 2020
- Hamburg - Hafencity
- Julingen
- Krefrath
- Krummenaab 2019
- Lemmental V3 Updated
- London
- Ruhrau V2
- Scunthorpe 1.1 (2016)
- X10 Berlin
- Yorkshire Counties 2.0

# Future plans
- Support all of our maps
- Error handling

# Vehicles that have been tried but doesn't work as AI
- Volvo 7700A - does not start moving after the stops
- Volvo 9900 - does not start moving after the stops

# Known bugs
## Missing checks
Currenly it doesn't check if you have bus or repaint installed
# Development
## Important notes
### unix2dos on data
After adding new files to `data`, make sure they are all in dos format:
```
find data/ -type f -print0 | xargs -0 unix2dos
```

### use `/r/n` instead of `/n`
Due to windows being stupid, use `/r/n` instead of `/n`

## Using VSCode for development

> Following extensions must be installed
> - [Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

1. Download latest backend dev image, using `docker pull gitlab.skeyl.io:5001/pms/backend-main:latest-dev`

2. Open VSCode.

    **Option A)** When you open this image folder in VSCode, use command `Remote Container: Reopen in container` or if `.devcontainer.json` file has changed, the option changes to `Remote Container: Rebuild and and open in container`.

    **Option B)** When you open this image folder in VSCode, a popup will open that it has detected `.devcontainer.json` file, do you want to open this project in Container?.
    After opening it, it may detect that `.devcontainer.json` has changed, in that case it reccomends rebuilding the container.

# Repository
## Pipeline
We are using https://gitlab.com/gitlab-ci-utils/gitlab-releaser to create releases automatically.

## Used
### UI
https://github.com/asticode/go-astilectron together with https://github.com/asticode/go-astilectron-bundler and https://github.com/asticode/go-astilectron-bootstrap

# Data
OMSI Data has been moved to https://gitlab.com/gogroupvirtual/data-omsi repository.
