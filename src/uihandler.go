package main

import (
	"encoding/json"
	"fmt"

	"github.com/asticode/go-astilectron"
	bootstrap "github.com/asticode/go-astilectron-bootstrap"
)

func handleUIMessages(window *astilectron.Window, message bootstrap.MessageIn) (payload interface{}, err error) {
	switch message.Name {
	case "checkSettings":
		if !checkSettings() {
			// window.Resize(800, 630)
			payload = "settings.html"
			return
		}
		// window.Resize(800, 320)
		payload = "data.html"
		return

	case "getSettings":
		settingsJSON, _ := json.Marshal(settings)
		payload = string(settingsJSON)
		return

	case "saveSettings":
		var omsiPath string
		if len(message.Payload) > 0 {
			json.Unmarshal(message.Payload, &omsiPath)
		}

		if !saveSettings(omsiPath) {
			fmt.Println("Error: Unable to save setting")
			payload = "Error: Unable to save setting"
			return
		}

		//indow.Resize(800, 320)
		payload = "index.html"
		fmt.Println(payload)
		return

	case "dataVersionCheck":
		payload = dataVersionCheck()
		return

	case "beginDataFolderSync":
		go beginDataFolderSync()
		payload = true
		return

	case "openMainMenu":
		// window.Resize(800, 630)
		payload = "main.html"
		return

	case "openSettings":
		// window.Resize(800, 630)
		payload = "settings.html"
		return

	case "openMapInstall":
		// window.Resize(800, 630)
		payload = "mapinstall.html"
		return

	case "loadMaps":
		maps = initMaps()
		mapsJSON, _ := json.Marshal(maps)
		payload = string(mapsJSON)
		return

	case "mapInstall":
		var mapName string
		if len(message.Payload) > 0 {
			if err = json.Unmarshal(message.Payload, &mapName); err != nil {
				payload = err
				return
			}
		}

		go mapSelected(mapName, "install")

		payload = true
		return

	case "submitMapExtras":
		var options string
		if len(message.Payload) > 0 {
			if err = json.Unmarshal(message.Payload, &options); err != nil {
				payload = err
				return
			}
		}

		payload = installMapExtra(options)
		return

	case "getMapData":
		selectedMapJSON, _ := json.Marshal(selectedMap)
		payload = string(selectedMapJSON)
		return

	case "getTrafficPacks":
		result := loadTrafficPacks()

		if result == "succesful" {
			payload = getMapTrafficPacks()
			return
		}
		payload = result
		return

	case "submitTrafficPacks":
		var options string
		if len(message.Payload) > 0 {
			if err = json.Unmarshal(message.Payload, &options); err != nil {
				payload = err
				return
			}
		}

		payload = installMapTraffic(options)
		return

	case "getBuses":
		result := loadBuses()

		if result == "succesful" {
			payload = getMapBuses()
			return
		}
		payload = result
		return

	case "submitBuses":
		var options string
		if len(message.Payload) > 0 {
			if err = json.Unmarshal(message.Payload, &options); err != nil {
				payload = err
				return
			}
		}

		payload = installMapBuses(options)
		return

	case "openMapUninstall":
		//window.Resize(800, 630)
		payload = "mapuninstall.html"
		return

	case "mapUninstall":
		var mapName string
		if len(message.Payload) > 0 {
			if err = json.Unmarshal(message.Payload, &mapName); err != nil {
				payload = err
				return
			}
		}
		go mapSelected(mapName, "uninstall")

		payload = true
		return

	case "openDepotSync":
		// window.Resize(800, 360)
		payload = "depotsync.html"
		return

	case "beginDepotSync":
		go depotSync()
		payload = true
		return

	case "versionCheck":
		payload = versionCheck()
		return

	}
	return
}
