package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/gookit/color"
)

type trafficPack struct {
	Name   string                  `json:"name"`
	Folder string                  `json:"folder"`
	Types  []trafficPackTypeStruct `json:"types"`
}

var trafficPacks []trafficPack

type trafficPackTypeStruct struct {
	Name         string   `json:"name"`
	FriendlyName string   `json:"friendlyname"`
	Vehicles     []string `json:"vehicles"`
	ParkList     []string `json:"parklist,omitempty"`
}

type mapTrafficPack struct {
	GroupName  string `json:"groupname"`
	OptionName string `json:"optionname"`
	Type       string `json:"type"`
	Folder     string `json:"folder"`
	Selected   bool   `json:"selected"`
}

var selectedMapTrafficPacks []mapTrafficPack

var ailist string
var buslist string
var parklist string

// Bus struct
type Bus struct {
	Name              string   `json:"name"`
	Description       string   `json:"description"`
	Path              string   `json:"path"`
	RepaintName       string   `json:"repaintName"`
	MapType           string   `json:"mapType"`
	RecommendedGroups []string `json:"reccomendedGroups"`
	Groups            []string `json:"groups"`
	Installed         bool     `json:"installed,omitempty"`
}

var buses []Bus

type busGroups struct {
	ID        string `json:"id"`
	GroupName string `json:"groupname"`
	FleetNo   int    `json:"fleetno"`
	Buses     []Bus  `json:"buses"`
}

var selectedMapBuses []busGroups

var selectedBuses []struct {
	Bus     string `json:"bus"`
	GroupID string `json:"groupid"`
}

func loadTrafficPacks() string {
	if doesExist("./data/trafficpacks.json") {
		fileData, err := ioutil.ReadFile("./data/trafficpacks.json")
		if err != nil {
			color.Red.Print(err)
			return "Error: Unable to load trafficpacks"
		}

		err = json.Unmarshal(fileData, &trafficPacks)
		if err != nil {
			color.Red.Println("error:", err)
			return "Error: Unable to parse trafficpacks"
		}
		return "succesful"
	}
	color.Red.Println("error: No trafficpacks file")
	return "Error: No trafficpacks file"
}

func getMapTrafficPacks() string {
	selectedMapTrafficPacks = nil

	for _, group := range selectedMap.TrafficGroups {
		for _, traffictype := range group.Types {
			for _, trafficpack := range trafficPacks {
				for _, trafficpacktype := range trafficpack.Types {
					if trafficpacktype.FriendlyName == traffictype {
						selectedMapTrafficPacks = append(selectedMapTrafficPacks, mapTrafficPack{
							GroupName:  group.Name,
							OptionName: trafficpack.Name + " - " + trafficpacktype.Name,
							Type:       trafficpacktype.FriendlyName,
							Folder:     trafficpack.Folder,
							Selected:   false,
						})
					}
				}
			}
		}
	}

	selectedMapTrafficPacksJSON, _ := json.Marshal(selectedMapTrafficPacks)
	return string(selectedMapTrafficPacksJSON)
}

func getTrafficPackValues(folder string, trafficpacktypename string, requesttype string) []string {
	for _, trafficpack := range trafficPacks {
		if trafficpack.Folder == folder {
			for _, trafficpacktype := range trafficpack.Types {
				if trafficpacktype.FriendlyName == trafficpacktypename {
					if requesttype == "vehicles" {
						return trafficpacktype.Vehicles
					} else if requesttype == "parklist" {
						return trafficpacktype.ParkList
					}
				}
			}
		}
	}

	return nil
}

func installMapTraffic(optionsJSON string) string {
	err := json.Unmarshal([]byte(optionsJSON), &selectedMapTrafficPacks)
	if err != nil {
		fmt.Println("error:", err)
		return "Error: Unable to parse input"
	}

	for _, group := range selectedMap.TrafficGroups {
		var alistGroup string
		for _, traffictype := range group.Types {
			for _, trafficpack := range selectedMapTrafficPacks {
				if trafficpack.Selected && trafficpack.Type == traffictype && trafficpack.GroupName == group.Name {
					fmt.Println("Copying " + trafficpack.Folder + "'s " + traffictype + " into memory")
					vehiclesData := getTrafficPackValues(trafficpack.Folder, traffictype, "vehicles")

					if vehiclesData != nil {
						alistGroup += strings.Join(vehiclesData, "\r\n") + "\r\n"
					}
					parklistData := getTrafficPackValues(trafficpack.Folder, traffictype, "parklist")

					if parklistData != nil {
						fmt.Println("Copying " + trafficpack.Folder + "'s parklist into memory")

						parklist += strings.Join(parklistData, "\r\n") + "\r\n"
					}
				}
			}
		}

		if len(alistGroup) > 0 {
			ailist += "[aigroup_2]\r\n"
			ailist += group.Name + "\r\n"
			ailist += "\r\n"

			ailist += alistGroup

			ailist += "[end]\r\n"
			ailist += "\r\n"
		}

	}

	if len(parklist) > 0 {
		writeToFile(mapPath+"/parklist_p.txt", parklist)
	} else {
		copyFile("./data/original/replaced_data/"+selectedMap.FolderName+"/parklist_p.txt", mapPath)
	}

	return "successful"
}

func loadBuses() string {
	if doesExist("./data/buses.json") {
		fileData, err := ioutil.ReadFile("./data/buses.json")
		if err != nil {
			color.Red.Print(err)
			return "Error: Unable to load buses"
		}

		err = json.Unmarshal(fileData, &buses)
		if err != nil {
			color.Red.Println("error:", err)
			return "Error: Unable to parse buses"
		}
		return "succesful"
	}
	color.Red.Println("error: No buses file")
	return "Error: No buses file"
}

func getMapBuses() string {
	selectedMapBuses = nil

	for _, bus := range buses {
		if bus.MapType == selectedMap.MapType {
			for _, busGroup := range bus.Groups {
				// Check if group exists
				groupExists := false
				var groupKey int

				for selectedkey, existingGroups := range selectedMapBuses {
					if existingGroups.ID == busGroup {
						groupExists = true
						groupKey = selectedkey
					}
				}

				bus.Installed = doesExist(settings.Omsipath + "/vehicles/" + bus.Path)

				if groupExists {
					selectedMapBuses[groupKey].Buses = append(selectedMapBuses[groupKey].Buses, bus)
				} else {
					var fleetNo int
					var groupName string

					if busGroup == "citySmall" {
						fleetNo = 800
						groupName = "GOBus City Micro"
					}
					if busGroup == "city12" {
						fleetNo = 1200
						groupName = "GOBus City 12m"
					}
					if busGroup == "city15" {
						fleetNo = 1500
						groupName = "GOBus City 15m"
					}
					if busGroup == "city18" {
						fleetNo = 1800
						groupName = "GOBus City 18m"
					}
					if busGroup == "city20" {
						fleetNo = 2000
						groupName = "GOBus City 20m"
					}
					if busGroup == "cityDD" {
						fleetNo = 2200
						groupName = "GOBus City DD"
					}
					if busGroup == "interSmall" {
						fleetNo = 900
						groupName = "GOBus Inter Micro"
					}
					if busGroup == "inter10" {
						fleetNo = 1100
						groupName = "GOBus Inter Small"
					}
					if busGroup == "inter12" {
						fleetNo = 1300
						groupName = "GOBus Inter 12m"
					}
					if busGroup == "inter15" {
						fleetNo = 1600
						groupName = "GOBus Inter 15m"
					}
					if busGroup == "inter18" {
						fleetNo = 1900
						groupName = "GOBus Inter 18m"
					}
					if busGroup == "coach" {
						fleetNo = 1700
						groupName = "GOBus Coaches"
					}

					var emptybus []Bus

					selectedMapBuses = append(selectedMapBuses, busGroups{
						ID:        busGroup,
						GroupName: groupName,
						FleetNo:   fleetNo,
						Buses:     append(emptybus, bus),
					})

				}
			}
		}
	}
	selectedMapBusesJSON, _ := json.Marshal(selectedMapBuses)
	return string(selectedMapBusesJSON)
}

func installMapBuses(optionsJSON string) string {
	err := json.Unmarshal([]byte(optionsJSON), &selectedBuses)
	if err != nil {
		fmt.Println("error:", err)
		return "Error: Unable to parse input"
	}

	for _, group := range selectedMapBuses {
		groupBuses := 0

		for _, mapGroup := range selectedMap.EnabledGroups {
			if mapGroup == group.ID {
				buslist += "[aigroup_depot]\r\n"
				buslist += group.GroupName + "\r\n"
				buslist += selectedMap.HofFile + "\r\n"
				buslist += "\r\n"

				for _, selectedBus := range selectedBuses {
					if selectedBus.GroupID == group.ID {
						for _, bus := range buses {
							if selectedBus.Bus == bus.Name {
								buslist += "[aigroup_depot_typgroup_2]\r\n"
								buslist += "vehicles/" + bus.Path + "\r\n"

								for a := 0; a < 20; a++ {
									buslist += fmt.Sprint(group.FleetNo) + "	GO " + fmt.Sprint(group.FleetNo) + "	" + bus.RepaintName + "\r\n"
									group.FleetNo++
								}

								buslist += "[end]\r\n"
								buslist += "\r\n"
								groupBuses++
							}
						}
					}
				}
			}
		}

		if groupBuses == 0 {
			mapRequiresGroup := false
			for _, mapGroup := range selectedMap.EnabledGroups {
				if mapGroup == group.ID {
					mapRequiresGroup = true
				}
			}
			if mapRequiresGroup == true {
				return "Error: Group " + group.GroupName + " does not have any buses selected"
			}
		}
	}
	writeAiListsFile()
	return "successful"
}

func writeAiListsFile() {
	if len(ailist) > 0 {
		editFile(mapPath+"/ailists.cfg", "{{REPLACE_CARS}}", ailist)
	} else {
		originalAiTraffic := readFromFile("./data/original/ai_traffic/" + selectedMap.FolderName + ".txt")
		editFile(mapPath+"/ailists.cfg", "{{REPLACE_CARS}}", originalAiTraffic)
	}

	editFile(mapPath+"/ailists.cfg", "{{REPLACE_BUSES}}", buslist)
}
