module gitlab.com/gogroupvirtual/omsi-map-overhaul-generator

go 1.17

require (
	github.com/asticode/go-astikit v0.38.0
	github.com/asticode/go-astilectron v0.29.0
	github.com/asticode/go-astilectron-bootstrap v0.4.14
	github.com/cavaliergopher/grab/v3 v3.0.1
	github.com/dustin/go-humanize v1.0.0
	github.com/gen2brain/go-unarr v0.1.2
	github.com/gookit/color v1.5.0
)

require (
	github.com/akavel/rsrc v0.10.2 // indirect
	github.com/asticode/go-astilectron-bundler v0.7.12 // indirect
	github.com/asticode/go-bindata v1.0.0 // indirect
	github.com/sam-kamerer/go-plister v1.2.0 // indirect
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	golang.org/x/sys v0.0.0-20220503163025-988cb79eb6c6 // indirect
)
