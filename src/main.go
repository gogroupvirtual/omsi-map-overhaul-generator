package main

import (
	"C"
	"flag"
	"os"

	"github.com/asticode/go-astilectron"
)

// Vars injected via ldflags by bundler
var (
	AppName            string
	BuiltAt            string
	VersionAstilectron string
	VersionElectron    string
)

// Var injected with build flags
var (
	fs         = flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	appVersion string
)

type settingsStruct struct {
	Omsipath string
}

var (
	settings  settingsStruct
	uiAddress string
	debug     = fs.Bool("d", false, "enables the debug mode")
	mainUI    *astilectron.Window
)

func main() {
	// Parse flags
	fs.Parse(os.Args[1:])

	openMainUI()
}
