FROM golang:1.17.8

# Configure to reduce warnings and limitations as instruction from official VSCode Remote-Containers.
# See https://code.visualstudio.com/docs/remote/containers-advanced#_reducing-dockerfile-build-warnings.
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils 2>&1

# Verify git, process tools, lsb-release (common in install instructions for CLIs) installed.
RUN apt-get -y install git iproute2 procps lsb-release

# Install Go tools.
RUN apt-get update \
    # Install other tools.
    go get -u -v \
        golang.org/x/tools/cmd/gopls \
        github.com/sqs/goreturns \
        github.com/mdempsky/gocode \
        github.com/uudashr/gopkgs/cmd/gopkgs \
        github.com/ramya-rao-a/go-outline \
        github.com/acroca/go-symbols \
        golang.org/x/tools/cmd/guru \
        golang.org/x/tools/cmd/gorename \
        github.com/go-delve/delve/cmd/dlv \
        github.com/stamblerre/gocode \
        github.com/rogpeppe/godef \
        golang.org/x/tools/cmd/goimports \
        golang.org/x/lint/golint 2>&1

# Install stuff required GUI development and build
RUN apt-get install -y libgtk-3-dev libwebkit2gtk-4.0-dev gcc-mingw-w64

# Tools for pipeline
RUN apt-get install -y p7zip-full

# Clean up.
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

# Revert workaround at top layer.
ENV DEBIAN_FRONTEND=dialog

# Add developer user
#RUN groupadd -g 1000 developer
#RUN useradd developer -u 1000 -g 1000  -ms /bin/bash \
#    && passwd -d developer \
#    && chown developer:developer /home/developer \
#    && addgroup developer sudo \
#    && addgroup developer staff
#RUN echo 'developer:developer' | chpasswd
#RUN chmod 755 /home/developer/
#RUN chown -R developer:developer /go

# Expose service ports.
EXPOSE 8000