package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	bootstrap "github.com/asticode/go-astilectron-bootstrap"
)

type depotfile struct {
	Name  string   `json:"name"`
	Buses []string `json:"buses"`
}

var depotFiles []depotfile

func depotSync() {
	depotSyncStatusChanged("Beginning sync of .hof files")

	var vehicleFolder string = settings.Omsipath + "/Vehicles"
	if loadDepotFiles() == false {
		depotSyncStatusChanged("Error: Unable to load depot files config")
	}

	for _, depotFile := range depotFiles {
		for _, bus := range depotFile.Buses {
			if doesExist(vehicleFolder + "/" + bus) {
				depotSyncStatusChanged("Copying file for " + bus)
				copyFolder("./data/depot-files/"+depotFile.Name, vehicleFolder+"/"+bus)
			}
		}
	}
	depotSyncStatusChanged("Depot file sync completed")
}

func loadDepotFiles() bool {
	if doesExist("./data/depotfiles.json") {
		fileData, err := ioutil.ReadFile("./data/depotfiles.json")
		if err != nil {
			fmt.Print(err)
			return false
		}

		err = json.Unmarshal(fileData, &depotFiles)
		if err != nil {
			fmt.Println("error:", err)
			return false
		}
		return true
	}

	fmt.Println("error: No depotfiles file")
	return false
}

func depotSyncStatusChanged(status string) {
	bootstrap.SendMessage(mainUI, "depotSyncStatusUpdate", status)
}
