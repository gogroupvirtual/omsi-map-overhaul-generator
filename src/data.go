package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	bootstrap "github.com/asticode/go-astilectron-bootstrap"
	"github.com/gen2brain/go-unarr"
	"github.com/gookit/color"
)

type dataonlineVersion struct {
	VersionV2     int    `json:"Version_V2"`
	DownloadUrlV2 string `json:"DownloadUrl_V2"`
}

type datalocalVersion struct {
	VersionV2 int `json:"Version_V2"`
}

var onlineDataVersion dataonlineVersion
var localDataVersion datalocalVersion

func dataVersionCheck() bool {
	fmt.Println("----------------------------------")
	fmt.Println("Checking your data folder version")
	getOnlineDataVersion()
	getLocalDataVersion()

	fmt.Println("Current version: " + color.Gray.Sprint(localDataVersion.VersionV2) + " | Online version: " + color.Gray.Sprint(onlineDataVersion.VersionV2))
	if localDataVersion.VersionV2 == onlineDataVersion.VersionV2 || localDataVersion.VersionV2 > onlineDataVersion.VersionV2 {
		return true
	}
	return false
}

func getOnlineDataVersion() (int, error) {
	resp, err := http.Get("https://gitlab.com/gogroupvirtual/data-omsi/-/raw/master/data/version.json")
	if err != nil {
		return 0, err
	}

	defer resp.Body.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	respByte := buf.Bytes()
	if err := json.Unmarshal(respByte, &onlineDataVersion); err != nil {
		fmt.Print(err)
		return 0, err
	}

	return onlineDataVersion.VersionV2, nil
}

func getLocalDataVersion() int {
	if doesExist("./data/version.json") {
		fileData, err := ioutil.ReadFile("./data/version.json")
		if err != nil {
			fmt.Print(err)
		}

		err = json.Unmarshal(fileData, &localDataVersion)
		if err != nil {
			fmt.Println("error:", err)
		}
	} else {
		localDataVersion.VersionV2 = 0
	}

	return localDataVersion.VersionV2
}

func beginDataFolderSync() bool {
	if doesExist("./data/") {
		dataFolderStatusChanged("Deleting old data folder")
		os.RemoveAll("./data/")
	}
	dataFolderStatusChanged("Downloading new data folder")
	downloadWithProgress("./data.zip", onlineDataVersion.DownloadUrlV2, "dataDownloadUpdate")

	dataFolderStatusChanged("Unpack downloaded folder....")

	// Use https://github.com/gen2brain/go-unarr to unpack because that does not break unicode characters
	a, err := unarr.NewArchive("./data.zip")
	if err != nil {
		panic(err)
	}
	defer a.Close()

	a.Extract("./data-temp")

	dataFolderStatusChanged("Cleaning up....")
	os.Rename("./data-temp/", "./data")
	//Clean up
	os.Remove("./data.zip")
	os.RemoveAll("./data-temp/")
	dataFolderStatusChanged("Data folder updated")

	return true
}

func dataFolderStatusChanged(status string) {
	bootstrap.SendMessage(mainUI, "dataDownloadStatusUpdate", status)
}
