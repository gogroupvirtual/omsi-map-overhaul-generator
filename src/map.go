package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	bootstrap "github.com/asticode/go-astilectron-bootstrap"
	"github.com/gookit/color"
)

type mapStruct struct {
	Name          string   `json:"name"`
	FolderName    string   `json:"folderName"`
	HofFile       string   `json:"hofFile"`
	MapType       string   `json:"mapType"`
	EnabledGroups []string `json:"enabledGroups"`
	TrafficGroups []struct {
		Name  string   `json:"name"`
		Types []string `json:"types"`
	} `json:"trafficGroups"`
	ExtraPacks []struct {
		Name               string `json:"name"`
		DetectFile         string `json:"detectFile"`
		CopyFolder         string `json:"copyFolder"`
		CopyFolderOriginal string `json:"copyFolderOriginal"`
	} `json:"extraPacks,omitempty"`
}

var maps []mapStruct

var selectedMap mapStruct

var mapPath string

type mapOptionStruct struct {
	ID    string `json:"id"`
	Value string `json:"value"`
}

var mapOptions []mapOptionStruct

type mapExtraStruct struct {
	ID    string `json:"id"`
	Value bool   `json:"value"`
}

var mapExtras []mapExtraStruct

func initMaps() []mapStruct {
	loadMaps()

	return maps
}

func mapStatusChanged(status string) {
	bootstrap.SendMessage(mainUI, "mapStatusUpdate", status)
}

func loadMaps() {
	if doesExist("./data/maps.json") {
		fileData, err := ioutil.ReadFile("./data/maps.json")
		if err != nil {
			fmt.Print(err)
		}

		err = json.Unmarshal(fileData, &maps)
		if err != nil {
			fmt.Println("error:", err)
		}
	} else {
		color.Red.Println("error: No maps file")
	}
}

func mapSelected(mapName string, selectedAction string) {
	var mapFound = false

	for _, row := range maps {
		if row.Name == mapName {
			selectedMap = row
			mapFound = true
		}
	}

	if mapFound == false {
		mapStatusChanged("Error: Incorrect map")
	}

	mapPath = settings.Omsipath + "/maps/" + selectedMap.FolderName

	if doesExist(mapPath) {
		if selectedAction == "install" || selectedAction == "uninstall" {
			// Remove old data formats
			if doesExist(mapPath + "/chrono/GO Group Virtual") {
				os.RemoveAll(mapPath + "/chrono/GO Group Virtual")
			}
			if doesExist(mapPath + "/chrono/zzz_GO Group Virtual") {
				os.RemoveAll(mapPath + "/chrono/zzz_GO Group Virtual")
			}

			mapStatusChanged("Copying original files.....")
			copyFolder("./data/original/"+selectedMap.FolderName, mapPath)
		}

		if selectedAction == "install" {
			installMap()
		} else if selectedAction == "uninstall" {
			uninstallMap()
		}
	} else {
		mapStatusChanged("Error: Map not installed")
	}
}

func installMap() {
	mapStatusChanged("Copying map specific files....")
	copyFolder("./data/core/"+selectedMap.FolderName, mapPath)

	mapStatusChanged("Core map files installed")

	if selectedMap.Name == "Fictional Szcezin" {
		mapStatusChanged("Copying map fixes for Fictional Szcezin....")
		copyFolder("./data/extra/[Fixes] Fikcyjny Szczecin map fixes", settings.Omsipath)
	}
	if selectedMap.ExtraPacks != nil {
		for _, extraPack := range selectedMap.ExtraPacks {
			if doesExist(mapPath + extraPack.DetectFile) {
				mapStatusChanged("Copying files for " + extraPack.Name)
				copyFolder(extraPack.CopyFolder, mapPath)
				fmt.Println("Installed emily " + extraPack.Name + " files")
			}
		}
	}

	mapStatusChanged("coreinstalled")
}

func uninstallMap() {
	mapStatusChanged("Recovering original map files....")
	copyFolder("./data/original/replaced_data/"+selectedMap.FolderName, mapPath)
	mapStatusChanged("Core map files installed")

	if selectedMap.ExtraPacks != nil {
		for _, extraPack := range selectedMap.ExtraPacks {
			if doesExist(mapPath + extraPack.DetectFile) {
				mapStatusChanged("Copying original files for " + extraPack.Name)
				copyFolder(extraPack.CopyFolderOriginal, mapPath)
				fmt.Println("Recovered " + extraPack.Name + " files")
			}
		}
	}

	mapStatusChanged("Overhaul removed")
}

func installMapExtra(optionsJSON string) string {
	err := json.Unmarshal([]byte(optionsJSON), &mapExtras)
	if err != nil {
		fmt.Println("error:", err)
		return "Error: Unable to parse input"
	}

	var drivers string
	var humans string
	var driverCounter int = 0
	var humanCounter int = 0

	for _, option := range mapExtras {
		var optionType string
		var fileData string

		fmt.Println(option.ID)

		if option.ID == "extraDriverHafen" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Hafencity DLC/drivers.txt")
		} else if option.ID == "extraDriver6Berlin" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Human DLC 6 - Berlin/drivers.txt")
		} else if option.ID == "extraDriver6Hamburg" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Human DLC 6 - Hamburg/drivers.txt")
		} else if option.ID == "extraDriver6London" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Human DLC 6 - London/drivers.txt")
		} else if option.ID == "extraDriver8Aachen" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Human DLC 8 - Aachen/drivers.txt")
		} else if option.ID == "extraDriver8Berlin" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Human DLC 8 - Berlin/drivers.txt")
		} else if option.ID == "extraDriver8Hamburg" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Human DLC 8 - Hamburg/drivers.txt")
		} else if option.ID == "extraDriver8London" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Human DLC 8 - London/drivers.txt")
		} else if option.ID == "extraDriver8Muenchen" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Human DLC 8 - Muenchen/drivers.txt")
		} else if option.ID == "extraDriver8Ruhrgebiet" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Human DLC 8 - Ruhrgebiet_Gladbeck/drivers.txt")
		} else if option.ID == "extraDriver8Wuppertal" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Human DLC 8 - Wuppertal/drivers.txt")
		} else if option.ID == "extraDriver12" && option.Value == true {
			optionType = "drivers"
			fileData = readFromFile("./data/extra/[Driver] Human DLC 12 - Asian/drivers.txt")
		} else if option.ID == "extraPassStan" && option.Value == true {
			optionType = "humans"
			fileData = readFromFile("./data/extra/[Passengers] Standard/humans.txt")
		} else if option.ID == "extraPassHum3DE" && option.Value == true {
			optionType = "humans"
			fileData = readFromFile("./data/extra/[Passengers] Human DLC 3 DE/humans.txt")
		} else if option.ID == "extraPassHum3EN" && option.Value == true {
			optionType = "humans"
			fileData = readFromFile("./data/extra/[Passengers] Human DLC 3 EN/humans.txt")
		} else if option.ID == "extraPassHum5DE" && option.Value == true {
			optionType = "humans"
			fileData = readFromFile("./data/extra/[Passengers] Human DLC 5 DE/humans.txt")
		} else if option.ID == "extraPassHum5EN" && option.Value == true {
			optionType = "humans"
			fileData = readFromFile("./data/extra/[Passengers] Human DLC 5 EN/humans.txt")
		} else if option.ID == "extraPassHum6DE" && option.Value == true {
			optionType = "humans"
			fileData = readFromFile("./data/extra/[Passengers] Human DLC 6 DE/humans.txt")
		} else if option.ID == "extraPassHum6EN" && option.Value == true {
			optionType = "humans"
			fileData = readFromFile("./data/extra/[Passengers] Human DLC 6 EN/humans.txt")
		} else if option.ID == "extraPassHum8DE" && option.Value == true {
			optionType = "humans"
			fileData = readFromFile("./data/extra/[Passengers] Human DLC 8 DE/humans.txt")
		} else if option.ID == "extraPassHum8EN" && option.Value == true {
			optionType = "humans"
			fileData = readFromFile("./data/extra/[Passengers] Human DLC 8 EN/humans.txt")
		} else if option.ID == "extraPassHum12" && option.Value == true {
			optionType = "humans"
			fileData = readFromFile("./data/extra/[Passengers] Human DLC 12/humans.txt")
		}

		fmt.Println(optionType)

		if optionType == "drivers" {
			if driverCounter > 0 {
				drivers += "\r\n"
			}
			drivers += fileData
			driverCounter++
		} else if optionType == "humans" {
			if humanCounter > 0 {
				humans += "\r\n"
			}
			humans += fileData
			humanCounter++
		}
	}

	if drivers != "" {
		fmt.Println("Writing to drivers file")
		deleteFile(mapPath + "/drivers.txt")
		writeToFile(mapPath+"/drivers.txt", drivers)
	}

	if humans != "" {
		fmt.Println("Writing to humans file")
		deleteFile(mapPath + "/humans.txt")
		writeToFile(mapPath+"/humans.txt", humans)
	}

	return "successful"
}
