var maps

$( document ).ready(function(){
    // This will wait for the astilectron namespace to be ready
    document.addEventListener('astilectron-ready', function() {
        getMaps()

        $('#mainmenu').click(function () {
            checkSettings()
        })

        $('#mapSelect').change(function () {
            selectMap($(this).val())
        })

        function getMaps() {
            astilectron.sendMessage({"name": "loadMaps"}, function(result) {
                if (result.payload) {
                    maps = JSON.parse(result.payload)
        
                    drawMapSelector()
                }
            })
        }
        
        function drawMapSelector() {
            for (const key in maps) {
                $('#mapSelect')
                    .append('<option value="' + maps[key].name +  '">' + maps[key].name +  '</option> ')
            }
        
            $('#loading').fadeOut()
            $('#mapSelector').fadeIn()
        }
        
        function selectMap(map) {
            $('#loading-text').text("Beginning overhaul uninstall for " + map + "")
            $('#mapSelector').fadeOut()
            $('#loading').fadeIn()
        
            astilectron.sendMessage({"name": "mapUninstall","payload": map}, function(result) {
                if (result.payload) {
                    // Listen for astilectron.onMessage mapStatusUpdate
                } else {
                    $('#loading-text').text("Unknown error occured")
                    $('#loader').fadeOut()
                }
            })
        }

        astilectron.onMessage(function(message) {
            switch (message.name) {
                case "mapStatusUpdate":
                    if (message && message.payload && message.payload == "Overhaul removed") {
                        $('#loading-text').text(message.payload)
                        $('#loader').fadeOut()
                        $('#install-progress').fadeOut()
                    } else if (message && message.payload) {
                        $('#install-progress').text(message.payload).show()
                    }
            }
        });
    })
})
