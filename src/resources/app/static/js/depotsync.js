var settings

$( document ).ready(function(){
    // This will wait for the astilectron namespace to be ready
    document.addEventListener('astilectron-ready', function() {
        $('#save').click(function () {
            $('#loading-text').text("Beginning sync")
            $("#loader").show()
            $('#loading').fadeIn()
            $('#confirmation').fadeOut()
            depotSyncStart()
        });
    
        $('#mainmenu').click(function () {
            openMainMenu()
        })

        function depotSyncStart() {
            astilectron.sendMessage({"name": "beginDepotSync"}, function(result) {
                if (result.payload) {
                    // Listen for astilectron.onMessage depotSyncStatusUpdate
                } else {
                    $('#loading').fadeOut()
                    $('#confirmationtext').text("Sync failed, would you like to retry?")
                    $('#confirmation').fadeIn()
                }
            })
        }

        astilectron.onMessage(function(message) {
            switch (message.name) {
                case "depotSyncStatusUpdate":
                    if (message && message.payload && message.payload == "Depot file sync completed") {
                        $("#loader").hide()
                        $('#loading-text').text("Sync completed")
            
                        $('#confirmationtext').hide()
                        $('#save').hide()
                        $('#confirmation').fadeIn()
                        ('#install-progress').fadeOut()
                    } else if (message && message.payload) {
                        $('#install-progress').text(message.payload).show()
                    }
            }
        });
    })
})