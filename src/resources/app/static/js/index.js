$( document ).ready(function(){
    // This will wait for the astilectron namespace to be ready
    document.addEventListener('astilectron-ready', function() {
        $('#loading-text').text('Checking settings')

        checkSettings()
    })
})