var settings

$( document ).ready(function(){
    // This will wait for the astilectron namespace to be ready
    document.addEventListener('astilectron-ready', function() {
        loadSettings()

        $('#save').click(function () {
            $('#errorBox').text("").hide()
            let omsiPath = $('#omsiExePath').val()
    
            astilectron.sendMessage({"name": "saveSettings","payload": omsiPath}, function(result) {
                if (result.payload && result.payload.startsWith('Error')) {
                    $('#errorBox').text("Error: " + result.payload).show()
                } else if (result.payload) {
                    $(location).attr('href', result.payload);
                }
            })
        });
    
        $('#mainmenu').click(function () {
            $(location).attr('href', "index.html");
        })

        function loadSettings() {
            astilectron.sendMessage({"name": "getSettings"}, function(result) {
                if (result.payload) {
                    settings = JSON.parse(result.payload)
                }
        
                $('#omsiExePath').val(settings.Omsipath)
            })
        }
    })
})