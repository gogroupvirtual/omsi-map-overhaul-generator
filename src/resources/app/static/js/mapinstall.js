var maps
var selectedMapName
var selectedMap

var trafficPacks

var buses
var showAllMapCompatibleBuses = false
var selectedBuses = []

var questions = []

$(document).ready(function() {
    // This will wait for the astilectron namespace to be ready
    document.addEventListener('astilectron-ready', function() {
        getMaps()

        $('#mainmenu').click(function() {
            checkSettings()
        })

        $('#mapSelect').change(function() {
            selectMap($(this).val())
        })

        $('#mapExtraSubmit').click(function() {
            submitMapExtra()
        })

        $('#mapTrafficPacksSubmit').click(function() {
            submitTrafficPacksSelect()
        })

        $('#toggleAllBuses').click(function() {
            showAllMapCompatibleBuses = $(this).prop('checked')
            selectBuses()
        })

        $('#mapBusesSubmit').click(function() {
            submitBusesSelect()
        })

        $('#runDepotSync').click(function() {
            openDepotSync()
        })

        function getMaps() {
            astilectron.sendMessage({ "name": "loadMaps" }, function(result) {
                if (result.payload) {
                    maps = JSON.parse(result.payload)

                    drawMapSelector()
                }
            })
        }

        function drawMapSelector() {
            for (const key in maps) {
                $('#mapSelect')
                    .append('<option value="' + maps[key].name + '">' + maps[key].name + '</option> ')
            }

            $('#loading').fadeOut()
            $('#mapSelector').fadeIn()
        }

        function selectMap(map) {
            $('#loading-text').text("Beginning overhaul install for " + map + "")
            $('#mapSelector').fadeOut()
            $('#loading').fadeIn()

            astilectron.sendMessage({ "name": "mapInstall", "payload": map }, function(result) {
                if (result.payload && result.payload == true) {
                    selectedMapName = map
                        // Listen for astilectron.onMessage mapStatusUpdate
                } else if (result.payload) {
                    $('#loading-text').text(result.payload)
                    $('#loader').fadeOut()
                } else {
                    $('#loading-text').text("Unknown error occured")
                    $('#loader').fadeOut()
                }
            })
        }

        function displayPrompts() {
            mapExtraStep()
        }

        function mapExtraStep() {
            $('#errorBox').fadeOut()
            $('#loading').fadeOut()

            $('#mapExtra').show()
        }

        function submitMapExtra() {
            $('#loading-text').text("Submitting your choices")
            $('#mapExtra').fadeOut()
            $('#loading').fadeIn()

            let values = [
                { id: "extraDriverHafen", value: $('#extraDriverHafen').prop('checked') },
                { id: "extraDriver6Berlin", value: $('#extraDriver6Berlin').prop('checked') },
                { id: "extraDriver6Hamburg", value: $('#extraDriver6Hamburg').prop('checked') },
                { id: "extraDriver6London", value: $('#extraDriver6London').prop('checked') },
                { id: "extraDriver8Aachen", value: $('#extraDriver8Aachen').prop('checked') },
                { id: "extraDriver8Berlin", value: $('#extraDriver8Berlin').prop('checked') },
                { id: "extraDriver8Hamburg", value: $('#extraDriver8Hamburg').prop('checked') },
                { id: "extraDriver8London", value: $('#extraDriver8London').prop('checked') },
                { id: "extraDriver8Muenchen", value: $('#extraDriver8Muenchen').prop('checked') },
                { id: "extraDriver8Ruhrgebiet", value: $('#extraDriver8Ruhrgebiet').prop('checked') },
                { id: "extraDriver8Ruhrgebiet", value: $('#extraDriver8Ruhrgebiet').prop('checked') },
                { id: "extraDriver8Wuppertal", value: $('#extraDriver8Wuppertal').prop('checked') },
                { id: "extraPassStan", value: $('#extraPassStan').prop('checked') },
                { id: "extraPassHum3EN", value: $('#extraPassHum3EN').prop('checked') },
                { id: "extraPassHum3DE", value: $('#extraPassHum3DE').prop('checked') },
                { id: "extraPassHum5EN", value: $('#extraPassHum5EN').prop('checked') },
                { id: "extraPassHum5DE", value: $('#extraPassHum5DE').prop('checked') },
                { id: "extraPassHum6EN", value: $('#extraPassHum6EN').prop('checked') },
                { id: "extraPassHum6DE", value: $('#extraPassHum6DE').prop('checked') },
                { id: "extraPassHum8EN", value: $('#extraPassHum8EN').prop('checked') },
                { id: "extraPassHum8DE", value: $('#extraPassHum8DE').prop('checked') },
                { id: "extraPassHum12", value: $('#extraPassHum12').prop('checked') }
            ]

            astilectron.sendMessage({ "name": "submitMapExtras", "payload": JSON.stringify(values) }, function(result) {
                if (result.payload && result.payload == "successful") {
                    $('#loading-text').text("Successful")
                    $('#errorBox').fadeOut()
                    $('#mapExtra').fadeOut()
                    getSelectedMapData()
                } else if (result.payload) {
                    $('#errorBox').text(result.payload).show()
                    $('#loading').fadeOut()
                    $('#mapExtra').fadeIn()
                } else {
                    $('#errorBox').text("Unknown error occured").show()
                    $('#loading').fadeOut()
                    $('#mapExtra').fadeIn()
                }
            })
        }

        function getSelectedMapData() {
            $('#errorBox').fadeOut()
            $('#loading').fadeIn()
            $('#loading-text').text("Getting selected map data")

            astilectron.sendMessage({ "name": "getMapData" }, function(result) {
                if (result.payload) {
                    selectedMap = JSON.parse(result.payload)
                    getAvailableTrafficPacks()
                    $('#errorBox').fadeOut()
                } else {
                    $('#errorBox').text("Unknown error occured").show()
                    $('#loading').fadeOut()
                }
            })
        }

        function getAvailableTrafficPacks() {
            $('#errorBox').fadeOut()
            $('#loading').fadeIn()
            $('#loading-text').text("Getting available traffic packs")

            astilectron.sendMessage({ "name": "getTrafficPacks" }, function(result) {
                if (result.payload && result.payload.startsWith('Error')) {
                    $('#errorBox').text(result.payload).show()
                    $('#loading').fadeOut()
                } else if (result.payload) {
                    trafficPacks = JSON.parse(result.payload)
                    $('#errorBox').fadeOut()
                    selectMapTrafficPacks()
                } else {
                    $('#errorBox').text("Unknown error occured").show()
                    $('#loading').fadeOut()
                }
            })
        }

        function selectMapTrafficPacks() {
            $('#loading').fadeOut()
            let loadedGroupNames = []

            trafficPacks.forEach(function(trafficpack) {
                if (!loadedGroupNames.includes(trafficpack.groupname)) {
                    $('#mapTrafficPacks .options')
                        .append(`<p class="h3">` + trafficpack.groupname + `</p>`)

                    loadedGroupNames.push(trafficpack.groupname)
                }

                $('#mapTrafficPacks .options')
                    .append(`
                        <div class="form-check">
                            <input class="form-check-input trafficpackselect" type="checkbox" value="" id="` + trafficpack.folder + `" trafficpack="` + trafficpack.groupname + `" " trafficpacktype="` + trafficpack.type + `">
                            <label class="form-check-label" for="` + trafficpack.folder + `">
                                ` + trafficpack.optionname + `
                            </label>
                        </div>
                    `)

            })

            $('#mapTrafficPacks').fadeIn()
        }

        function submitTrafficPacksSelect() {
            $('#mapTrafficPacks').fadeOut()
            $('#loading').fadeIn()
            $('#loading-text').text("Submitting your traffic selection")

            $('.trafficpackselect').each(function(input, key) {
                $this = $(this)
                trafficPacks.forEach(function(trafficpack, key) {
                    if (trafficpack.folder == $this.attr('id') && trafficpack.groupname == $this.attr('trafficpack') && trafficpack.type == $this.attr('trafficpacktype')) {
                        trafficPacks[key].selected = $this.prop('checked')
                    }
                })
            })

            astilectron.sendMessage({ "name": "submitTrafficPacks", "payload": JSON.stringify(trafficPacks) }, function(result) {
                if (result.payload && result.payload.startsWith('Error')) {
                    $('#errorBox').text(result.payload).show()
                    $('#loading').fadeOut()
                    $('#mapTrafficPacks').fadeIn()
                } else if (result.payload) {
                    $('#mapTrafficPacks').fadeOut()
                    $('#errorBox').fadeOut()
                    getAvailableBuses()
                } else {
                    $('#errorBox').text("Unknown error occured").show()
                    $('#loading').fadeOut()
                    $('#mapTrafficPacks').fadeIn()
                }
            })
        }

        function getAvailableBuses() {
            $('#errorBox').fadeOut()
            $('#loading').fadeIn()
            $('#loading-text').text("Getting available buses")

            astilectron.sendMessage({ "name": "getBuses" }, function(result) {
                if (result.payload && result.payload.startsWith('Error')) {
                    $('#errorBox').text(result.payload).show()
                    $('#loading').fadeOut()
                } else if (result.payload) {
                    buses = JSON.parse(result.payload)
                    $('#errorBox').fadeOut()
                    selectBuses()
                } else {
                    $('#errorBox').text("Unknown error occured").show()
                    $('#loading').fadeOut()
                }
            })
        }

        function selectBuses() {
            $('#loading').fadeOut()
            $('#mapBuses .options').empty()

            buses.forEach(function(busgroup) {
                if (selectedMap.enabledGroups.includes(busgroup.id)) {
                    let busesInGroup = []

                    $('#mapBuses .options')
                        .append(`<p class="h3">` + busgroup.groupname + `</p>`)

                    busgroup.buses.forEach(function(bus) {
                        drawBus(bus, busgroup, false)
                        busesInGroup.push(bus.name)
                    })

                    if (showAllMapCompatibleBuses) {
                        includeAllBuses(busgroup.id, busesInGroup)
                    }
                }
            })

            $('#mapBuses').fadeIn()
        }

        function includeAllBuses(excludeGroupID, excludeBuses) {
            buses.forEach(function(busgroup) {
                if (excludeGroupID != busgroup.id) {
                    busgroup.buses.forEach(function(bus) {
                        if (!excludeBuses.includes(bus.name)) {
                            drawBus(bus, busgroup, true)
                        }
                    })
                }
            })
        }

        function drawBus(bus, busgroup, extended) {
            $('#mapBuses .options')
                .append(`
                    <div class="form-check ` + ((bus.reccomendedGroups.includes(busgroup.id)) ? `fw-bold` : ``) + `` + ((extended) ? `text-muted` : ``) + `">
                        <input class="form-check-input bussselect" type="checkbox" value="" id="` + bus.name + `" group-id="` + busgroup.id + `"` + ((!bus.installed) ? `disabled` : ``) + `>
                        <label class="form-check-label" for="` + bus.name + `">
                        ` + ((!bus.installed) ? `<span class="fw-bolder text-danger">NOT INSTALLED</span> ` : ``) + bus.name + ` 
                            <small class="text-muted">` + bus.description + `</small>
                        </label>
                    </div>
                `)
        }

        function submitBusesSelect() {
            $('#mapBuses').fadeOut()
            $('#loading').fadeIn()
            $('#loading-text').text("Submitting your traffic selection")

            $('.bussselect').each(function(input, key) {
                $this = $(this)
                if ($this.prop('checked')) {
                    selectedBuses.push({
                        bus: $this.attr('id'),
                        groupid: $this.attr('group-id')
                    })
                }
            })

            astilectron.sendMessage({ "name": "submitBuses", "payload": JSON.stringify(selectedBuses) }, function(result) {
                if (result.payload && result.payload.startsWith('Error')) {
                    $('#errorBox').text(result.payload).show()
                    $('#loading').fadeOut()
                    $('#mapBuses').fadeIn()
                } else if (result.payload) {
                    $('#mapBuses').fadeOut()
                    $('#errorBox').fadeOut()
                    $('#loading-text').text("Finalizing map overhaul install")
                    finalizeMapInstall()
                } else {
                    $('#errorBox').text("Unknown error occured").show()
                    $('#loading').fadeOut()
                    $('#mapBuses').fadeIn()
                }
            })
        }

        function finalizeMapInstall() {
            $('#loading').fadeOut()
            $('#finalizeMapInstall').fadeIn()
        }

        astilectron.onMessage(function(message) {
            switch (message.name) {
                case "mapStatusUpdate":
                    if (message && message.payload && message.payload == "coreinstalled") {
                        displayPrompts()
                    } else if (message && message.payload) {
                        $('#install-progress').text(message.payload).show()
                    }
            }
        });
    })
})