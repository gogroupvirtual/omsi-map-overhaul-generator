package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type gitlabReleaseStruct struct {
	Name    string `json:"name"`
	TagName string `json:"tag_name"`
}

var gitlabReleases []gitlabReleaseStruct

var onlineVersion string

func getOnlineVersions() {
	resp, err := http.Get("https://gitlab.com/api/v4/projects/gogroupvirtual%2Fomsi-map-overhaul-generator/releases")
	if err != nil {
		fmt.Print(err)
	}

	defer resp.Body.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	respByte := buf.Bytes()
	json.Unmarshal(respByte, &gitlabReleases)
}

func latestOnlineVersion() {
	getOnlineVersions()
	for _, release := range gitlabReleases {
		if release.TagName >= onlineVersion {
			onlineVersion = release.TagName
		}
	}
}

func versionCheck() string {
	latestOnlineVersion()

	fmt.Print("App version: " + appVersion + " | Latest version: " + onlineVersion)
	if appVersion != "" {
		if appVersion >= onlineVersion {
			return "Up to date"
		}
		return "Your version is out of date, latest version is " + onlineVersion
	}
	return ""
}
