$( document ).ready(function(){
    // This will wait for the astilectron namespace to be ready
    document.addEventListener('astilectron-ready', function() {
        /* function openMainMenu() {
            astilectron.sendMessage({"name": "openMainMenu"}, function(result) {
                if (result.payload && result.payload.startsWith('Error')) {
                    //Add error handling
                } else if (result.payload) {
                    $(location).attr('href', result.payload);
                }
            });  
        } */

        checkVersion()    

        function checkVersion() {
            $('#errorBox').hide()
            $('#loading-text').text("Checking data folder")
        
            astilectron.sendMessage({"name": "dataVersionCheck"}, function(result) {
                if (result.payload) {
                    $('#loading-text').text("Data folder is up to date")
                    openMainMenu()
                } else {
                    $('#loading-text').text("Your data folder is outdated, downloading update")
                    beginFolderSync()
                }
            })        
        } 
        
        function beginFolderSync() {
            astilectron.sendMessage({"name": "beginDataFolderSync"}, function(result) {
                if (result.payload) {
                    //Everything is okay, don't do nothing. Wait on dataDownloadStatusUpdate 
                } else {
                    $('#loading-text').text("")
                    $('#loader').hide()
                    $('#errorBox').text("Unable to download data folder").show()
                }
            })
        }

        astilectron.onMessage(function(message) {
            switch (message.name) {
                case "dataDownloadStatusUpdate":
                    if (message && message.payload && message.payload == "Data folder updated") {
                        checkVersion()
                    } else if (message && message.payload) {
                        $('#install-progress').text(message.payload).show()
                    }
            }
        });
    })
})