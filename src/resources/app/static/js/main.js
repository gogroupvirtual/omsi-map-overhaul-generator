$( document ).ready(function(){
    // This will wait for the astilectron namespace to be ready
    document.addEventListener('astilectron-ready', function() {
        doVersionCheck()

        $('#buttonMapInstall').click(function () {
            openMapInstall()
        });
        
        $('#buttonMapUninstall').click(function () {
            openMapUninstall()
        });
        
        $('#buttonDepotSync').click(function () {
            openDepotSync()
        });

        $('#buttonSettings').click(function () {
            openSettings()
        });

        function doVersionCheck() {
            astilectron.sendMessage({"name": "versionCheck"}, function(result) {
                if (result.payload) {
                    $('#version').text(result.payload)
                    if (result.payload != "Up to date" ) {
                        $('#version').addClass("text-danger").removeClass("text-muted")
                    }
                } else {
                    $('#version').text("Unable to check version")
                }
            })
        }
    })
})