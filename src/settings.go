package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/gookit/color"
)

func checkSettings() bool {
	readSettings()
	if doesExist(settings.Omsipath + "/Omsi.exe") {
		return true
	}
	return false
}

func readSettings() {
	// Check if settings file exists
	if doesExist("./settings.json") {
		fileData, err := ioutil.ReadFile("./settings.json")
		if err != nil {
			fmt.Print(err)
		}

		err = json.Unmarshal(fileData, &settings)
		if err != nil {
			color.Red.Println("error:", err)
		}
	} else {
		color.Red.Println("Settings file does not exist")
	}
}

func saveSettings(path string) bool {
	path = strings.Trim(path, "\r\n")

	setting := &settingsStruct{Omsipath: path}

	settingsJSON, err := json.MarshalIndent(setting, "", "    ")
	if err != nil {
		fmt.Println(err)
		return false
	}
	writeToFile("./settings.json", string(settingsJSON))
	return true
}
