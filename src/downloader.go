package main

import (
	"fmt"
	"os"
	"time"

	"github.com/cavaliergopher/grab/v3"
	"github.com/dustin/go-humanize"
)

var statusUpdater string

func downloadWithProgress(filepath string, fileURL string, statusCall string) {
	if statusCall != "" {
		statusUpdater = statusCall
	}

	downloadFile(filepath, fileURL)
	/* if err != nil {
		panic(err)
	} */
}

func downloadFile(filepath string, url string) {
	// create client
	client := grab.NewClient()
	req, _ := grab.NewRequest(filepath, url)

	// start download
	fmt.Printf("Downloading %v...\n", req.URL())
	resp := client.Do(req)
	fmt.Printf("  %v\n", resp.HTTPResponse.Status)

	// start UI loop
	t := time.NewTicker(500 * time.Millisecond)
	defer t.Stop()

Loop:
	for {
		select {
		case <-t.C:
			downloadProgress("Downloading...." + humanize.Bytes(uint64(resp.BytesComplete())) + " / " + humanize.Bytes(uint64(resp.Size())) + " (" + fmt.Sprintf("%.2f", 100*resp.Progress()) + "%)")

		case <-resp.Done:
			// download is complete
			break Loop
		}
	}

	// check for errors
	if err := resp.Err(); err != nil {
		fmt.Fprintf(os.Stderr, "Download failed: %v\n", err)
	}

	fmt.Printf("Download saved to ./%v \n", resp.Filename)
}

func downloadProgress(status string) {
	if statusUpdater != "" {
		downloadStatusUpdater(statusUpdater, status)
	}
}

func downloadStatusUpdater(action string, status string) {
	if action != "" {
		if action == "dataDownloadUpdate" {
			dataFolderStatusChanged(status)
		}
	}
}
