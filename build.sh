#!/usr/bin/env bash

package_version="9.0.0"

go install github.com/asticode/go-astilectron-bundler/astilectron-bundler@latest

cd src/
astilectron-bundler -ldflags X:main.appVersion=$package_version

cd ..