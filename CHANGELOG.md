# Changelog

## 0.9.0
- Rewrote the tool to not use our custom chrono events anymore
- Support for V2 data structure
- Added support for inter18 bus group

## 0.8.1
- Fix our chrono sometimes refusing to properly load if it's not the last folder

## 0.8.0
- Add auto-detect logic for Ahlheim & Laurenzbach Updated line addons
- Add support for Ahlheim & Laurenzbach Updated Linepack 2021

## 0.7.4
- Fix humans and drivers not being configured correctly

## 0.7.3
- Add support to all humans DLCs

## 0.7.2
- Fix tool not working in machines that previously had not run version prior to 0.7.0
- Updated go modules

## 0.7.1
- Adjust data downloader to get the latest download url from omsi-data gitlab repo like latest version

## 0.7.0
- Fix data download path
- Remove priority hardcoded logic (it's now written in data)
- Update to golang 1.17.8
- Fix data download and unpacking breaking unicode letters (breaking for example Städtedreieck path)

## 0.6.5
- Change how trafficpacks are stored and how ai-list (not buses) is created. Priorities are now based on category (most generic cars, least emergency vehicles etc)
- Fix empty bus groups being created in ai-list

## 0.6.4
- Split data into separate repository. Closes https://gitlab.com/gogroupvirtual/omsi-map-overhaul-generator/-/issues/24 

## 0.6.3
-  Fix bus group check bug that included also groups that map doesn't have

## 0.6.2
- Fix main menu buttons not being evenly spaced
- Increase the amount of maps displayed at once on map select on overhaul install & uninstall
- Fix Continue button spacing on ai-list and buses select
- Better explain what traffic vehicle types mean
- Fix wrong step loaded back if bus select stage fails
- Add description to trafficpack selection that you do not need to have anything selected at all
- Add check that at least 1 bus needs to be selected in each bus group
- Disable buses (and add text Not installed) for all buses that user doesn't have installed

## 0.6.1
- Fixed https://gitlab.com/gogroupvirtual/omsi-map-overhaul-generator/-/issues/19
- Move data download status from console to UI
- Move map ovheraul install & uninstall statuses to UI from console
- Rewrite downloader to display file progress (total file size and percentage)
- Disable console on production builds

## 0.6.0
- Replaced ui engine to `asticode/go-astilectron`, which uses electron to fix compatibility issues
- Closes https://gitlab.com/gogroupvirtual/omsi-map-overhaul-generator/-/issues/17
- Fixes https://gitlab.com/gogroupvirtual/omsi-map-overhaul-generator/-/issues/21
- Got rid of the annoying extra .dll files

> **Main .exe file renamed to `GO Group Virtual OMSI Tool.exe`**<br />
> Make sure to get rid of old .exe and .dll files!

## 0.5.0
- **Added first version of UI**
- Added latest version check for application itsefl
- Added option to specify which AI vehicles (not buses) you want in which group
- Disabled build process for 32bit version

> **Main .exe file renamed to `govirttool.exe`**

## 0.2.5
- Join together city 25 and city 20 categories
- Add support for London DLC

## 0.2.4
- Fix Coaches group not generated correctly into AI-list

## 0.2.3
- Add support for Hamburg Hafencity

## 0.2.2
- Fix mapType's not being loaded correctly, breaking support for UK maps
- Add option to view all buses supported by map instead of group specific buses

## 0.2.1
- Fix bugs in ailist file generation
- Fix wrong line endings on strings (needed \r\n instead of \n because of windows)
- Fix parklist file not being created

> Anybody using any versino of our tool before this version, you may delete those completely as they are now legacy.

## 0.2.0
- Rewrote the tool into an actual application
- Added automatic data folder updater/downloader
- Added dynamic methods for adding/changing/removing support for maps and vehicles

> Anybody using any versino of our tool before this version, you may delete those completely as they are now legacy.
